## 5G and IIoT Project

## File information

This folder contains following files : 

1. `app.py` - This is a python file which provides the implementation of required rest api using flask.

1. `requirements.txt` - This file contains the name of python modules used in the python script. We install these modules when building the container.

1. `Dockerfile` -  This file is used to build the docker container. 


## Run Application

1. Build the container

```bash
docker build -t akshay .
```

2. Run the container

```bash
docker run --rm -d -p 8080:5000 --name akshay-info akshay 
```