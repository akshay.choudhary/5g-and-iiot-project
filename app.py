from flask import Flask, jsonify
app = Flask(__name__)


@app.route("/v1/name")
def get_name():
	return jsonify("Akshay Choudhary")


@app.route("/v1/email")
def get_email():
	return jsonify("akshay.choudhary@campus.tu-berlin.de")


@app.route("/v1/mtknr")
def get_mtknr():
	return jsonify("390160")


@app.route("/v1/courseOfStudies")
def get_courseOfStudies():
	return jsonify("Master of Science (M.Sc.)")


@app.route("/v1/background")
def get_background():
	return jsonify(["5th Generation Mobile Networks", "Industrial Internet of Things", "5G and IIoT Seminar", "High Speed Network Technologies"])


@app.route("/v1/skills")
def get_skills():
	return jsonify([{'programming languages':['Java','Python','C++']},{'databases':['MySQL','NoSQL']},{'tools':['wireshark']}])


@app.route("/v1/topics")
def get_topics():
	return jsonify(["Test Open Baton with ElasTest ","Realization and testing of IoT appiication using ElasTest","Real-time Audio over AVB and TSN"])


if __name__ == "__main__":
	app.run(host="0.0.0.0", port=int("5000"), debug=True)
